package br.com.rollorafael.rom.controller;

import java.net.URI;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.rollorafael.rom.dto.LoginDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class LoginControllerTest {
	
	@Autowired
	private TestRestTemplate testRestTemplate;

	@Test
	public void shouldAuthenticateUserWithCorrectAccessData() {
		
		LoginDTO dto = new LoginDTO();
		dto.setUsername("rafael.rollo@rom.com.br");
		dto.setPassword("123456");
		
		RequestEntity<LoginDTO> request = RequestEntity
				.post(URI.create("/api/public/login"))
				.contentType(MediaType.APPLICATION_JSON).body(dto);
		
		ResponseEntity<String> response = testRestTemplate.exchange(request, String.class);
		
		Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
		Assert.assertTrue(!response.getBody().isEmpty());
		System.out.println(response.getBody());
	}
	
	@Test
	public void shouldntAuthenticateUserWithIncorrectAccessData() {
		
		LoginDTO dto = new LoginDTO();
		dto.setUsername("rafael.rollo@rom.com.br");
		dto.setPassword("654321");
		
		RequestEntity<LoginDTO> request = RequestEntity
				.post(URI.create("/api/public/login"))
				.contentType(MediaType.APPLICATION_JSON).body(dto);
		
		ResponseEntity<String> response = testRestTemplate.exchange(request, String.class);
		
		Assert.assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
		Assert.assertTrue(response.getBody() == null || response.getBody().isEmpty());
	}
}
