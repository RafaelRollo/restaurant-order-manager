package br.com.rollorafael.rom.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.rollorafael.rom.dto.LoginDTO;
import br.com.rollorafael.rom.security.TokenAuthenticationService;

@RestController
@CrossOrigin
public class LoginController {

	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private Environment environment;
	
	@PostMapping(value = "/api/public/login", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> authenticate(@RequestBody LoginDTO loginDTO) {

		String uri = getLocalPath() + "/api/login";
		RequestEntity<LoginDTO> request = RequestEntity
				.post(URI.create(uri))
				.contentType(MediaType.APPLICATION_JSON)
				.body(loginDTO);
		try {
			ResponseEntity<String> response = restTemplate.exchange(request, String.class);
			return ResponseEntity.ok(response.getHeaders().get(TokenAuthenticationService.AUTH_HEADER_NAME).get(0));
		
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}

	private String getLocalPath() {
		String port = environment.getProperty("local.server.port");
		return "http://localhost:" + port;
	}
}
