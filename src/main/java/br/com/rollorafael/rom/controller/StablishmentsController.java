package br.com.rollorafael.rom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.rollorafael.rom.model.Stablishment;
import br.com.rollorafael.rom.repository.StablishmentRepository;

@RestController("/stablishments")
@CrossOrigin
public class StablishmentsController {

	@Autowired
	private StablishmentRepository stablishmentRepository;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Iterable<Stablishment> list() {

		return stablishmentRepository.findAll();
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Stablishment> create(@RequestBody Stablishment stablishment) {

		Stablishment createdStablishment = stablishmentRepository.save(stablishment);
		return new ResponseEntity<Stablishment>(createdStablishment, HttpStatus.CREATED);
	}

}
