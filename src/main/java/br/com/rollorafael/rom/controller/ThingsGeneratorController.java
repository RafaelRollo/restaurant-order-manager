package br.com.rollorafael.rom.controller;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rollorafael.rom.model.Role;
import br.com.rollorafael.rom.model.User;

@RestController
@Transactional
public class ThingsGeneratorController {

	@PersistenceContext
	private EntityManager manager;

	@GetMapping("/generate/roles")
	public String insertRoles() {
		
		List<Role> roles = Arrays.asList( new Role("ROLE_ADMIN"),
				new Role("ROLE_STABLISHMENT_OWNER"),
				new Role("ROLE_STABLISHMENT_MANAGER"),
				new Role("ROLE_STABLISHMENT_EMPLOYEE"),
				new Role("ROLE_CLIENT"));
		
		roles.stream().forEach(manager::persist);
		return "roles ok";
	}
	
	@GetMapping("/generate/admin")
	public String insertAdmin() {
		
		Role role = new Role("ROLE_ADMIN");
		
		User user = new User();
		user.setEmail("rafael.rollo@rom.com.br");
		user.setPassword(new BCryptPasswordEncoder().encode("123456"));
		user.setNome("Rafael");
		user.addRole(role);
		
		manager.persist(user);
		return "admin ok";
	}
}
