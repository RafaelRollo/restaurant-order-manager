package br.com.rollorafael.rom.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.rollorafael.rom.model.Stablishment;

@Repository
public interface StablishmentRepository extends CrudRepository<Stablishment, Long> {

}
