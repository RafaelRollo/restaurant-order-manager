package br.com.rollorafael.rom.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import br.com.rollorafael.rom.model.User;

@Repository
public class UserRepository implements UserDetailsService {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		String jpql = "select u from User u where u.email = :email";
		
		try {
			return manager.createQuery(jpql, User.class)
					.setParameter("email", username)
					.getSingleResult();

		} catch (NoResultException e) {
			throw new UsernameNotFoundException("Usuário " + username + "não pode ser encontrado!", e);
		}
	}

}
