package br.com.rollorafael.rom.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.security.core.GrantedAuthority;

@Entity
@SuppressWarnings("serial")
public class Role implements GrantedAuthority{

	@Id
	private String name;

	/**
	 * @deprecated
	 */
	public Role() {}	
	
	public Role(String name) {
		this.name = name;
	}

	@Override
	public String getAuthority() {
		return this.getName();
	}
	
	public String getName() {
		return name;
	}
}
